#!/usr/bin/env python3

from office365.runtime.auth.authentication_context import AuthenticationContext
from office365.sharepoint.client_context import ClientContext
from office365.sharepoint.file import File
from datetime import date, timedelta
import xlrd, sys, re
from collections import defaultdict

def get_month_summary(target_date, session, verbose=None):
    ''' Get a summary of the works performed for the given month '''

    if verbose:
        print('Loading workbook...', end='\r')

    topdirURL = session['sharepoint_url']
    username = session['sp_username']
    password = session['sp_password']

    filename = session['wksched_path'] + target_date.strftime(session['fn_format'])
    projstatus = '/TZTraffic/Docs/Project Status.xlsx'

    ctx_auth = AuthenticationContext(topdirURL)
    if ctx_auth.acquire_token_for_user(username, password):
        context = ClientContext(topdirURL, ctx_auth)

    # First get the list of projects that have in-house TMP/TGSs
    f = context.web.get_file_by_server_relative_url(projstatus)
    context.load(f)
    try:
        context.execute_query()
        response = File.open_binary(context, projstatus)
        wb = xlrd.open_workbook(file_contents = response.content)
    except Exception as error:
        error = f'Error loading sharepoint document. Error: {error}'
        sys.exit(error)
    tmp_nums = list()

    # Look in all worksheets for TMP numbers
    for ws in wb.sheets():
        tmps = list(filter(lambda x: x, map(lambda x: x if isinstance(x, str) else str(int(x)), ws.col_values(1, 4))))
        regexp = re.compile(r'\d+')
        tmp_nums += [regexp.findall(t)[0] for t in tmps]

    # Now get the main Master Planner workbook.
    f = context.web.get_file_by_server_relative_url(filename)
    context.load(f)
    try:
        context.execute_query()
        response = File.open_binary(context, filename)
        wb = xlrd.open_workbook(file_contents = response.content)
    except Exception as error:
        error = f'Error loading sharepoint document. Error: {error}'
        sys.exit(error)

    summary_by_day = defaultdict(lambda : [])
    summary_by_tmp = defaultdict(lambda : [])
    summary_by_job = set()
    today = date.today()

    for sh in wb.sheets():
        if sh.nrows > 1 and sh.cell_value(0, 0).startswith('DATE') and (sh.cell_type(0, 1) == xlrd.XL_CELL_DATE):
            sh_date = xlrd.xldate.xldate_as_datetime(sh.cell_value(0,1), wb.datemode)
            if sh_date.date() > today:
                break
            if verbose:
                print(str(sh_date), end='\r')
            # Iterate over the rows to find each job bounds
            for rownum in range(sh.nrows):
                row = sh.row(rownum)
                if sh.cell_type(rownum, 0) == xlrd.XL_CELL_TEXT and row[0].value.strip() == 'JOB NO' and row[1].value.strip() == 'QUOTE NO' and not (xlrd.XL_CELL_ERROR in sh.row_types(rownum+1, start_colx=1, end_colx=4) or sh.cell_value(rownum+1, 1) == ''):
                    job = tuple(map(lambda i: str(int(i.value)) if i.ctype == xlrd.XL_CELL_NUMBER else str(i.value) if i.ctype != xlrd.XL_CELL_ERROR else '', sh.row(rownum+1)[:3]))
                    summary_by_day[sh_date].append(job)
                    summary_by_job.add(job)
                    jobno = regexp.findall(job[1])[0]
                    if jobno in tmp_nums:
                        summary_by_tmp[jobno].append(sh_date)
    return summary_by_day, summary_by_job, summary_by_tmp


if __name__ == "__main__":

    def get_settings():
        import sqlite3
        from cryptography.fernet import Fernet

        pwkey = b'703f6Q5j8vbIu2R1_4cHxIDmudrwXCBlFpRY6JFXHV8='
        settings = None
        
        with sqlite3.connect('db/crewalloc.db') as conn:
            c = conn.cursor()
            settings = dict(c.execute('select * from settings').fetchall())
        # Decrypt password
        f = Fernet(pwkey)
        settings['sp_password'] = f.decrypt(settings['sp_password']).decode()
        return settings
    
    target_date = date.today() - timedelta(days = date.today().day) # Last day of previous month
    # target_date = date.today()
    s1, s2, s3 = get_month_summary(target_date, get_settings(), True)

    print("\nBy TMP")
    for tmp in sorted(s3):
        print('{}: {}'.format(tmp, ', '.join([str(d.day) for d in s3[tmp]])))
    
    print("\nBy job")
    for job in sorted(s2, key=lambda x: x[1]):
        print('{}{} - {}'.format(job[0], job[1], job[2]))

    print("\nBy date")
    # print(repr(summary))
    for key in sorted(s1):
        print(key.strftime("%A, %d %B %Y"))
        for job in s1[key]:
            print('    {}'.format(', '.join(job)))
