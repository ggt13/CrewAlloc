var tag = document.createElement('script');
tag.id = 'iframe-demo';
tag.src = 'https://www.youtube.com/iframe_api';
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    console.log('In onYouTubeIframeAPIReady');
    player = new YT.Player('videoplayer', {
        playerVars: {
            controls: 0,
            loop: 1,
            origin: 'ppp198-151.static.internode.on.net'
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}
function onPlayerReady(event) {
    console.log('In onPlayerReady');
}
function onPlayerStateChange(event) {
    console.log('In onPlayerStateChange: ' + event.data);
}
$(function(){
    console.log('In jQuery document ready.');
    $(document).on('show hide', function (e) {
        console.log('The page is now', e.type === 'show' ? 'visible' : 'hidden');
        if(e.type === 'show') {
            player.playVideo();
        } else {
            player.pauseVideo();
        }
    });
});