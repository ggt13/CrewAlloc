$(function() {
    $('#inpbuttons input[type="submit"]').click(function(){
        $('#target_day').attr("value", $(this).attr("value"));
    });
    $('form').submit(function(e){
        $('input[type="submit"]').prop('disabled', true);
        $('a').addClass('disabled');
        $('i.fas').hide();
        $('#load-anim').removeClass("invisible");

        return true;
    });
    $('#project-select').change(function(){
        $(this).parents('form').submit();
    });
})