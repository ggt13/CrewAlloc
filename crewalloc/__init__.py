import os

from flask import Flask, render_template, url_for, request, session, flash, redirect, get_flashed_messages, send_file
from datetime import date, timedelta, datetime
from wtforms import Form, StringField, PasswordField, validators
from wtforms.ext.dateutil import fields
from crewalloc.tz_works import TZworks
from crewalloc.dwi_export import DWIexport
from pathlib import Path
from cryptography.fernet import Fernet, InvalidToken
import os, pickle

class DWIForm(Form):
    target_date = fields.DateField('Date', description='Target date for generating & showing the work instructions', render_kw={'class': 'form-control mx-sm-2', 'placeholder': 'Enter date', 'type': 'date'})

class SettingsForm(Form):
    sharepoint_url = StringField('Sharepoint URL', [validators.DataRequired()], description='URL for the Sharepoint site', render_kw={'class': 'form-control mx-sm-2', 'placeholder': 'Enter sharepoint URL', 'type': 'text'})
    wksched_path = StringField('Works Schedule path', [validators.DataRequired()], description='In the Windows File Explorer select the "Works Schedule" folder, click on "Copy path" then paste into this text field.', render_kw={'class': 'form-control mx-sm-2', 'placeholder': 'Enter "Works Schedule" path', 'type': 'text'})
    fn_format = StringField('Filename format', [validators.DataRequired()], description='Format of the Master Planner workbook filename (under the path above) generated from date.', render_kw={'class': 'form-control mx-sm-2', 'placeholder': 'Enter filename format', 'type': 'text'})
    sp_username = StringField('Sharepoint Username', [validators.DataRequired()], description='Username to use for accessing sharepoint.', render_kw={'class': 'form-control mx-sm-2', 'placeholder': 'Sharepoint username', 'type': 'text'})
    sp_password = PasswordField('Sharepoint Password', description='Password to use for accessing sharepoint. Leave blank to keep existing password.', render_kw={'class': 'form-control mx-sm-2', 'type': 'password', 'autocomplete': 'off'})

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'db/crewalloc.db'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.template_filter()
    def monthname(dateval):
        ''' Extract month name from dateval or integer month number '''
        if isinstance(dateval, date):
            return dateval.strftime('%B')
        elif isinstance(dateval, int):
            return date.today().replace(month=dateval,day=1).strftime('%B')
        else:
            raise ValueError('Invalid date value. Should be either date or int.')

    @app.template_filter()
    def nicedate(dateval):
        ''' return date in a nicely readable format '''
        return dateval.strftime('%A, %d %B %Y')

    @app.template_filter()
    def days(datelist):
        ''' return just the day numbers of a list of dates. '''
        return [str(d.day) for d in datelist]

    from . import db
    db.init_app(app)

    def export_dwi(works):
        ''' Export the daily work instructions PDF corresponding to the passed in works '''
        dwi_dir = Path(app.static_folder + '/DWI')
        dwi_dir.mkdir(exist_ok=True)
        dwi = DWIexport('L', 'mm', 'A4')
        for job in works.jobs:
            dwi.add_dwi_page(works.date.strftime("%A, %d %B %Y"), job, app.static_folder + '/tz_group_logo.png')
        filename = works.date.strftime('DWI/DWI_%Y%m%d.pdf')
        dwi.output(app.static_folder + '/' + filename, 'F')
        return url_for('static', filename=filename)

    @app.route('/', methods=['GET', 'POST'])
    def index():
        session.permanent = True
        form = DWIForm(request.form)

        if request.method == 'POST':
            if request.values['form-id'] == 'main':
                session['submit'] = request.values['target_day']
                return redirect(url_for('crew_alloc'))
            elif request.values['form-id'] == 'dwi':
                if form.validate():
                    session['dwi_date'] = request.values['target_date']
                    return redirect(url_for('dwi_day'))
            elif request.values['form-id'] == 'summary':
                return redirect(url_for('summary'))

        show_monday = (date.today().weekday() in [4, 5])

        # Load settings from database
        settings_entries = db.query_db('select * from settings;')
        for entry in settings_entries:
            if entry[0].endswith('assword'):
                f = Fernet(app.config['PASSWORD_KEY'])
                try:
                    session[entry[0]] = f.decrypt(entry[1].encode()).decode()
                except InvalidToken:
                    flash('Corrupt encrypted password. Re-enter correct password.')
                    return redirect(url_for('settings'))
            else:
                session[entry[0]] = entry[1]
        # If not all required settings are present then redirect to settings page.
        if not ('sharepoint_url' in session and 'wksched_path' in session and 'fn_format' in session and 'sp_username' in session and 'sp_password' in session):
            flash('Please set all items to valid values.')
            return redirect(url_for('settings'))
        
        return render_template('index.html', showmonday=show_monday, form=form)

    def get_cached_works(name, target_date, session):
        ''' Get works for day specified by arg name ("today", "tomorrow", or "monday").
            First check the database and if there's an entry there that's still recent use it, otherwise
            get the data from the excel workbook on sharepoint. '''
        # Get the last modified time from the database
        db_action = None
        w = TZworks(target_date, session=session)
        if w.error:
            return w
        
        entry = db.query_db('select * from sheets where sheetname = ?', [name], one=True)
        if entry is None:
            db_action = 'insert'
            w.get_works_by_date()
        else:
            # Check last modified time & actual date
            if w.last_modified > entry['last_modified'] or w.last_modified.date() != entry['last_modified'].date():
                db_action = 'update'
                w.get_works_by_date()
            else:
                w.jobs, w.absentees = pickle.loads(entry['sheetdata'])
                w.date = target_date
                w.worksheetloaded = 'cached'
        
        if w.error:
            return w

        if db_action == 'insert':
            # Save to database
            cur = db.get_db().execute('INSERT INTO sheets (sheetname, last_modified, sheetdata) values (?, ?, ?)', \
                (name, w.last_modified, pickle.dumps((w.jobs, w.absentees), 4)))
        elif db_action == 'update':
            cur = db.get_db().execute('UPDATE sheets SET last_modified = ?, sheetdata = ? WHERE sheetname = ?', \
                (w.last_modified, pickle.dumps((w.jobs, w.absentees), 4), name))
        if db_action:
            cur.close()
            db.get_db().commit()
        
        return w


    @app.route('/crew', methods=['GET'])
    def crew_alloc():
        if session['submit'].endswith('Today'):
            target_date = date.today()
            w = get_cached_works('today', target_date, session)
            if w.error:
                flash(w.error)
                return redirect(url_for('index'))
            else:
                # Export today's daily work instructions as pdf (first creating the subdirectory if necessary)
                try:
                    export_dwi(w)
                except Exception as error:
                    flash(f'Error creating worksheet PDF: {error}')

        elif session['submit'].endswith('Tomorrow'):
            target_date = date.today() + timedelta(days=1)
            w = get_cached_works('tomorrow', target_date, session)
            if w.error:
                flash(w.error)
                return redirect(url_for('index'))

        elif session['submit'].endswith('Monday'):
            days2mon = 7 - date.today().weekday()
            target_date = date.today() + timedelta(days=days2mon)
            w = get_cached_works('monday', target_date, session)
            if w.error:
                flash(w.error)
                return redirect(url_for('index'))
        
        # Write page to a file here.
        try:
            with open("crew_alloc_display.html.tmp", "w") as file:
                file.write(render_template('crew_alloc.html', works=w, export=True))
            os.replace('crew_alloc_display.html.tmp', 'crew_alloc_display.html')
        except Exception as error:
            flash(error)
        return render_template('crew_alloc.html', works=w, export=False)

    @app.route('/dwi/', methods=['GET'])
    def dwi_list():
        ''' Get all daily work instructions from the DWI directory and show them in a list
            from where each one can be viewed/downloaded.
        '''
        from collections import defaultdict

        thisyear = str(date.today().year)
        dwi_files = defaultdict(lambda : [])
        for file in Path(app.static_folder + '/DWI').glob('*.pdf'):
            if file.name[4:8] == thisyear:
                dwi_files[int(file.name[8:10])].append(file.name)

        return render_template('work_intructions.html', filesbymonth=dwi_files)

    @app.route('/dwi_day', methods=['GET'])
    def dwi_day():
        w = TZworks(datetime.strptime(session['dwi_date'], "%Y-%m-%d").date(), session=session)
        w.get_works_by_date()
        # Export the supplied date's daily work instructions as pdf (first creating the subdirectory if necessary)
        try:
            filename = export_dwi(w)
        except Exception as error:
            flash(f'Error creating worksheet PDF: {error}')
            return redirect(url_for('index'))

        return redirect(filename)

    @app.route('/logout')
    def logout():
        session.clear()
        flash('Logged out')
        return redirect(url_for('index'))

    @app.route('/summary')
    def summary():
        from crewalloc.works_summary import get_month_summary

        s1, s2, s3 = get_month_summary(date.today() - timedelta(days = date.today().day), session=session)
        return render_template('summary.html', summary=s1, byjob=sorted(s2, key = lambda x: x[1]), bytmp=s3)
    
    def get_day_details(dayid):
        daytimes = db.query_db('select jobdate,start_time,end_time,revenue from jobday where id = ?', [dayid], one=True)
        monthnum = daytimes[0].replace(day=1)
        crewday = db.query_db('select crewname,crewhours,cost from crewday where jobdayid = ?', [dayid])
        equipday = db.query_db('select equipname,cost from equipday where jobdayid = ?', [dayid])
        subcday = db.query_db('select subcontractor,cost from subcday where jobdayid = ?', [dayid])
        materialday = db.query_db('select material,amount,cost from materialday where jobdayid = ?', [dayid])
        return({'times': daytimes, 'crew': crewday, 'equipment': equipday, 'subcontractor': subcday, 'material': materialday, 'month': monthnum})
    
    @app.route('/project', methods=['GET', 'POST'])
    def project_details():
        projnum = pdetails = alldaydetails = jobmaterials = jobsubs = allmdetails = mdetails = allsdetails = sdetails = None
        if request.method == 'POST':
            projnum = request.values['project']
            # Get total project costs.
            revtotal = db.query_db('select sum(revenue) from jobday where jobid = ?', [projnum], one=True)[0] or 0
            crewcost = db.query_db('select sum(cost),sum(crewhours) from crewday inner join jobday on crewday.jobdayid = jobday.id where jobday.jobid = ?', [projnum], one=True)
            equipcost = db.query_db('select sum(cost) from equipday inner join jobday on equipday.jobdayid = jobday.id where jobday.jobid = ?', [projnum], one=True)[0] or 0
            subccost = db.query_db('select sum(cost) from subcday inner join jobday on subcday.jobdayid = jobday.id where jobday.jobid = ?', [projnum], one=True)[0] or 0
            materialcost = db.query_db('select sum(cost) from materialday inner join jobday on materialday.jobdayid = jobday.id where jobday.jobid = ?', [projnum], one=True)[0] or 0
            # Get active job days.
            jobdays = db.query_db('select id,jobdate from jobday where jobid = ? order by jobdate', [projnum])
            # Get materials used list.
            jobmaterials = db.query_db('select distinct material from materialday inner join jobday on materialday.jobdayid = jobday.id where jobday.jobid = ? order by material', [projnum])
            # Get subcontractors list.
            jobsubs = db.query_db('select distinct subcontractor from subcday inner join jobday on subcday.jobdayid = jobday.id where jobday.jobid = ? order by subcontractor', [projnum])
            pdetails = {'revenue': revtotal, 'crewcost': crewcost, 'equipcost': equipcost, 'subccost': subccost, 'materialcost': materialcost, 'jobdays': jobdays}
            pdetails['PMs'] = {'AM': 'Andrew Morris', 'DK': 'Dean Keith', 'JB': 'Jon Billing'}
            if 'jobdayid' in request.values:
                # Get specific day details.
                alldaydetails = [get_day_details(request.values['jobdayid'])]
            elif 'jobdayall' in request.values:
                # Get details for all days
                alldaydetails = []
                for dayid in [j['id'] for j in jobdays]:
                    alldaydetails.append(get_day_details(dayid))
            elif 'material' in request.values:
                mdetails = {'name': request.values['material']}
                mdetails['details'] = db.query_db('select jobdate,cost from materialday inner join jobday on materialday.jobdayid = jobday.id where material = ? and jobid = ? order by jobdate', [request.values['material'], projnum])
                allmdetails = [mdetails]
            elif 'materialall' in request.values:
                allmdetails = []
                for material in [m['material'] for m in jobmaterials]:
                    mdetails = {'name': material}
                    mdetails['details'] = db.query_db('select jobdate,cost from materialday inner join jobday on materialday.jobdayid = jobday.id where material = ? and jobid = ? order by jobdate', [material, projnum])
                    allmdetails.append(mdetails)
            elif 'subc' in request.values:
                sdetails = {'name': request.values['subc']}
                sdetails['details'] = db.query_db('select jobdate,cost from subcday inner join jobday on subcday.jobdayid = jobday.id where subcontractor = ? and jobid = ? order by jobdate', [request.values['subc'], projnum])
                allsdetails = [sdetails]
            elif 'subcall' in request.values:
                allsdetails = []
                for subc in [s['subcontractor'] for s in jobsubs]:
                    sdetails = {'name': subc}
                    sdetails['details'] = db.query_db('select jobdate,cost from subcday inner join jobday on subcday.jobdayid = jobday.id where subcontractor = ? and jobid = ? order by jobdate', [subc, projnum])
                    allsdetails.append(sdetails)
        
        projects = dict([(n[0], tuple(n[1:])) for n in db.query_db('select * from job')])
        return render_template('project.html', projects=projects, projnum=projnum, projdetails=pdetails, alldaydetails=alldaydetails, jobmaterials=jobmaterials, jobsubs=jobsubs, allmdetails=allmdetails, allsdetails=allsdetails)

    def load_settings(form):
        ''' Load the form field values from the database.
        '''
        for item in form:
            entry = db.query_db('select * from settings where setting = ?', [item.name], one=True)
            if entry and not item.name.endswith('assword'):     # Don't populate any password fields
                item.data = entry[1]

    def save_settings(form):
        ''' Save the settings as defined by the passed in form elements as a set of key, value pairs.
        '''
        for item in form:
            # If the field is left blank then don't update the database.
            if not item.data:
                continue
            session[item.name] = item.data
            # encrypt any passwords
            if item.name.endswith('assword'):
                f = Fernet(app.config['PASSWORD_KEY'])
                item.data = f.encrypt(item.data.encode()).decode()
            # Create or update the value
            entry = db.query_db('select * from settings where setting = ?', [item.name], one=True)
            if entry is None:
                cur = db.get_db().execute('INSERT INTO settings (setting, val) values (?, ?)', (item.name, item.data))
            else:
                cur = db.get_db().execute('UPDATE settings SET val = ? WHERE setting = ?', (item.data, item.name))
            cur.close()
        db.get_db().commit()

    @app.route('/settings', methods=['GET', 'POST'])
    def settings():
        form = SettingsForm(request.form)
        if request.method == "GET":
            load_settings(form)
        elif request.method == 'POST' and form.validate():
            filename = ''
            fp = Path(request.values['wksched_path'].strip('"').replace('\\', '/'))
            try:
                ind = fp.parts.index('MC - Documents')
            except ValueError:
                filename = str(fp)
            else:
                filename = str(Path('/Shared Documents').joinpath(Path(*fp.parts[ind+1:])))
            form['wksched_path'].data = filename
            save_settings(form)
            flash('Settings saved', 'success')
        return render_template('settings.html', form=form)
    
    @app.route('/forecast', methods=['GET'])
    def forecast():
        from crewalloc.weather import get_forecast
        fc = get_forecast('VIC_PT144')      # Get today's forecast for Pakenham (from the BOM)
        return render_template('forecast.html', forecast=fc)

    @app.route('/<whoops>')
    def error(whoops):
        return render_template('error.html')

    return app

app = create_app()
