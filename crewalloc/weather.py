#!/usr/bin/python3

from ftplib import FTP
from io import StringIO
from datetime import datetime
import xml.etree.ElementTree as ET

def get_forecast(aac, target_date = datetime.now()):
    ''' Get the weather forecast from BOM for the supplied forecast area code (aac).
    '''
    forecast = dict()
    weather_host = 'ftp.bom.gov.au'
    weather_dir = '/anon/gen/fwo/'
    forecast_product = 'IDV10753.xml'

    xmlbuffer = StringIO()

    with FTP(weather_host) as ftp:
        ftp.login()
        ftp.cwd(weather_dir)
        ftp.retrlines('RETR ' + forecast_product, xmlbuffer.write)
    
    docroot = ET.fromstring(xmlbuffer.getvalue())
    fcast = docroot.find(f'forecast/area[@aac="{aac}"]')
    if fcast:
        # print(f'Weather for {fcast.attrib["description"]}...')
        for fp in fcast:
            stime = datetime.fromisoformat(fp.attrib['start-time-local'])
            etime = datetime.fromisoformat(fp.attrib['end-time-local'])
            if stime < target_date.replace(tzinfo=stime.tzinfo) < etime:
                forecast = dict([(e.attrib['type'], e.text) for e in fp])
                break
        forecast['aac_name'] = fcast.attrib["description"]
    
    return forecast

if __name__ == "__main__":
    for (key, val) in get_forecast('VIC_PT144').items():
        print(f'{key}: {val}')