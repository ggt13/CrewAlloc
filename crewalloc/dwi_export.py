#!/usr/bin/env python3
from fpdf import FPDF
import platform

class DWIexport(FPDF):
    pms = {'DK': ('Dean Keith', '0419 357 356'), 'AM': ('Andrew Morris', '0419 357 078'), 'JB': ('Jon Billing', '0418 829 308')}

    def __init__(self, *args):
        super().__init__(*args)
        if platform.system() == 'Linux':
            self.add_font('Ubuntu', '', '/usr/share/fonts/truetype/ubuntu/Ubuntu-R.ttf', True)
            self.add_font('UbuntuB', '', '/usr/share/fonts/truetype/ubuntu/Ubuntu-B.ttf', True)
            self.add_font('UbuntuBI', '', '/usr/share/fonts/truetype/ubuntu/Ubuntu-BI.ttf', True)
        elif platform.system() == 'Windows':
            self.add_font('Ubuntu', '', 'C:/Windows/Fonts/Ubuntu-R.ttf', True)
            self.add_font('UbuntuB', '', 'C:/Windows/Fonts/Ubuntu-B.ttf', True)
            self.add_font('UbuntuBI', '', 'C:/Windows/Fonts/Ubuntu-BI.ttf', True)

    def add_dwi_page(self, jobdate='', job={}, logo_filename=None):
        LineHeight = 5.7
        self.add_page()
        self.set_font('UbuntuB', '', 24)
        self.set_fill_color(217, 225, 242)
        y = self.get_y()
        self.cell(0, 15, txt = 'DAILY WORK INSTRUCTIONS', align = 'C', fill = True, ln = 1)
        if logo_filename:
            self.image(logo_filename, 240, y, h = 15)
        self.set_font('UbuntuB', '', 11)
        self.set_fill_color(226, 239, 218)
        self.cell(145, LineHeight, txt = 'PROJECT DETAILS', align = 'C', border = 1, fill = True)
        self.cell(0, LineHeight, txt = 'CONTACTS', align = 'C', border = 1, fill = True, ln = 1)
        self.set_fill_color(255)

        self.cell(50, LineHeight, 'DATE:', 1)
        self.cell(95, LineHeight, jobdate, 1)
        self.cell(50, LineHeight, 'Project Manager:', 1)

        # Populate project manager information if it's known
        if job['job'] in DWIexport.pms:
            self.cell(50, LineHeight, DWIexport.pms[job['job']][0], 1)
            self.cell(0, LineHeight, DWIexport.pms[job['job']][1], 1, ln = 1)
        else:
            self.cell(50, LineHeight, '', 1)
            self.cell(0, LineHeight, '', 1, ln = 1)

        self.cell(50, LineHeight, 'PROJECT:', 1)
        self.cell(95, LineHeight, job['project'], 1)
        self.cell(50, LineHeight, 'Operations Manager:', 1)
        self.cell(50, LineHeight, 'Luke Gillon', 1)
        self.set_font('Ubuntu', '', 11)
        self.cell(0, LineHeight, '0412 240 508', 1, ln = 1)
        self.set_font('UbuntuB', '', 11)

        self.cell(50, LineHeight, 'PROJECT NO:', 1)
        self.cell(95, LineHeight, job['job'] + job['quote'], 1)
        self.cell(50, LineHeight, 'Safety Manager:', 1)
        self.cell(50, LineHeight, 'Dean Mills', 1)
        self.set_font('Ubuntu', '', 11)
        self.cell(0, LineHeight, '0409 958 302', 1, ln = 1)

        self.cell(0, LineHeight, ln = 1)  # Blank line

        self.set_fill_color(217, 225, 242)
        self.set_text_color(48, 84, 150)
        self.set_font('UbuntuBI', '', 11)

        self.cell(70, LineHeight, 'Crew Leader:', 1, align = 'C', fill = True)
        self.cell(70, LineHeight, 'Plant:', 1, align = 'C', fill = True)
        self.cell(0, LineHeight, 'Daily Work Instructions:', 1, align = 'C', fill = True, ln = 1)

        self.set_text_color(0)
        self.set_font('Ubuntu', '', 11)
        self.cell(70, LineHeight, job['crew'][0][0], 1)    # crew leader
        x = self.get_x()
        y = self.get_y()
        self.multi_cell(70, LineHeight, '\n'.join([name for name,cost in job['equipment']]), 1) # plant list
        self.set_xy(x + 70, y)
        y = self.get_y()
        self.multi_cell(0, LineHeight, job['scope'], 'R')   # work instructions
        self.set_y(y)
        self.ln(LineHeight)
        self.set_text_color(48, 84, 150)
        self.set_font('UbuntuBI', '', 11)
        self.cell(70, LineHeight, 'Crew', 1, align = 'C', fill = True)
        self.set_text_color(0)
        self.set_font('Ubuntu', '', 11)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)

        x = self.get_x()
        y = self.get_y()
        self.multi_cell(70, LineHeight, '\n'.join([name for name,hrs,cost in job['crew'][1:]]), 1)  # crew list (without leader)
        self.set_xy(x + 70, y)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 'R', ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.set_text_color(48, 84, 150)
        self.set_font('UbuntuBI', '', 11)
        self.cell(0, LineHeight, 'Risks:', 1, align = 'C', fill = True, ln = 1)

        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 1, ln = 1)

        self.set_text_color(48, 84, 150)
        # self.set_font('UbuntuBI', '', 11)
        self.cell(140, LineHeight, 'Subcontractors:', 1, align = 'C', fill = True)
        self.cell(0, LineHeight, '', 1, ln = 1)

        self.set_text_color(0)
        self.set_font('Ubuntu', '', 11)
        x = self.get_x()
        y = self.get_y()
        subs = [name for name,cost in job['subcontractors']]
        self.multi_cell(70, LineHeight, '\n'.join(subs[0:2]), 1)    # subcontractors (part 1)
        self.set_xy(x + 70, y)
        self.multi_cell(70, LineHeight, '\n'.join(subs[3:5]), 1)          # subcontractors (part 2)
        self.set_xy(x + 140, y)
        self.cell(0, LineHeight, '', 1, ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 1, ln = 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(70, LineHeight, '', 1)
        self.cell(0, LineHeight, '', 1, ln = 1)

        self.set_text_color(48, 84, 150)
        self.set_font('UbuntuBI', '', 11)
        self.cell(0, LineHeight, 'Project Sign Off', 1, ln = 1, align = 'C', fill = True)
        self.set_text_color(0)

        self.set_font('UbuntuB', '', 11)
        self.cell(25, LineHeight, 'Start Time', 1)
        self.set_font('Ubuntu', '', 11)
        self.cell(14, LineHeight, str(job['times'][0]), 1)
        self.cell(20, LineHeight, '', 1)
        self.set_font('UbuntuB', '', 11)
        self.cell(25, LineHeight, 'Finish Time', 1)
        self.set_font('Ubuntu', '', 11)
        self.cell(14, LineHeight, str(job['times'][1]), 1)
        self.cell(20, LineHeight, '', 1)
        self.set_font('UbuntuB', '', 11)
        self.cell(0, LineHeight, 'Notes:', 'R', align = 'L', ln = 1)
        self.cell(39, LineHeight, 'Works Completed:', 1)
        self.cell(22, LineHeight, 'Y', 1, align = 'C')
        self.cell(23, LineHeight, 'N', 1, align = 'C')
        self.cell(0, LineHeight, '', 1, ln = 1)

        self.cell(39, LineHeight, 'Reinstatements:', 1)
        self.cell(22, LineHeight, 'Y', 1, align = 'C')
        self.cell(23, LineHeight, 'N', 1, align = 'C')
        self.cell(0, LineHeight, '', 1, ln = 1)

        self.set_font('UbuntuB', '', 14)
        self.cell(70, LineHeight * 1.7, 'Crew Leader Signature:', 1)
        self.cell(95, LineHeight * 1.7, '', 1)
        self.cell(25, LineHeight * 1.7, 'Date:', 1)
        self.cell(0, LineHeight * 1.7, '', 1)

if __name__ == "__main__":
    d = DWIexport('L', 'mm', 'A4')
    d.add_dwi_page("Today's date")
    d.output('dwi_export.pdf', 'F')
