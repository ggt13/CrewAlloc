# Works class - load a sharepoint excel workbook and extract the works details
# for a particular day

from office365.runtime.auth.authentication_context import AuthenticationContext
from office365.sharepoint.client_context import ClientContext
from office365.sharepoint.file import File
from office365.runtime.client_request_exception import ClientRequestException
from datetime import date
from dateutil import parser
from collections import defaultdict
import xlrd

# Test url
# "C:\Users\GlennTrigg\Mayberry Constructions\MC - Documents\New set up\Works\Works Schedule\2019\12-December\Master Planner - December 19.xlsx"
# https://mayberryconstruct.sharepoint.com/Shared%20Documents/Works/Construction/Works%20Schedule/2019/7-July/Master%20Planner%20-%20July%2019.xlsx?web=1

class TZworks:

    # topdirURL = 'https://mayberryconstruct.sharepoint.com'

    def __init__(self, targetDate, session=None, loadOnInit=False):

        # Initialise instance variables
        self.jobs = []
        self.workbook_filename = ''
        self.worksheetloaded = ''
        self.error = ''
        self.date = None
        self.wb = None
        self.target_date = targetDate
        self.topdirURL = session['sharepoint_url']
        self.wksched_path = session['wksched_path']
        self.fn_format = session['fn_format']

        ctx_auth = AuthenticationContext(self.topdirURL)
        try:
            if not session or 'sp_username' not in session:
                self.error = 'Username not supplied.'
                return
            else:
                if ctx_auth.acquire_token_for_user(session['sp_username'], session['sp_password']):
                    self.context = ClientContext(self.topdirURL, ctx_auth)
                else:
                    self.error = f"Unable to log in to sharepoint. Username = {session['sp_username']}, password supplied."
                    return
        except Exception as e:
            self.error = f'Error logging in to Sharepoint. Check URL, Username and Password. (Error: {e})'
            return

        self._workbook_filename(targetDate)
        try:
            f = self.context.web.get_file_by_server_relative_url(self.workbook_filename)
        except AttributeError as error:
            self.error = f"Can't access workbook named '{self.workbook_filename}': {error}"
            return
        try:
            self.context.load(f)
            self.context.execute_query()
        except Exception as error:
            self.error = f"Couldn't load workbook named '{self.workbook_filename}': {error}"
            return
        self.last_modified = parser.isoparse(f.properties['TimeLastModified']).replace(tzinfo=None)
        if loadOnInit:
            self.get_works_by_date()
    
    def _workbook_filename(self, target_date):
        self.workbook_filename = self.wksched_path + target_date.strftime(self.fn_format)

    @classmethod
    def totime(cls, d):
        if not isinstance(d, float):
            return d
        h = d * 24
        m = int(h % int(h) * 60)
        return f'{int(h):02}:{m:02}'
        # return "%02d:%02d" % (int(h), m)
    
    def open_workbook(self):
        response = File.open_binary(self.context, self.workbook_filename)
        self.wb = xlrd.open_workbook(file_contents = response.content)

        # Perform some sanity checks on the workbook.
        sheetnames = [s.name for s in self.wb.sheets()]
        if 'Overview' not in sheetnames:
            self.error = f'Sheet "Overview" is not present in workbook {self.workbook_filename}'
        if sheetnames[2:33] != [str(n) for n in range(1,32)]:
            self.error = f'Day sheets are not all present or in correct order in workbook {self.workbook_filename}'
        # Why does the below code work for user 'pi' but not user 'www-data'????
        #
        # try:
        #     with File.open_binary(self.context, self.workbook_filename) as response:
        #         self.wb = xlrd.open_workbook(file_contents = response.content)
        # except Exception as error:
        #     self.error = 'Error loading sharepoint document. Error: %s' % str(error)
    
    def get_all_projects(self):
        ''' Get the list of projects and associated data from the "Projects Main" sheet.
        '''
        import re

        if not self.wb:
            self.open_workbook()
            if self.error:
                return None
        
        try:
            ws = self.wb.sheet_by_name('Projects Main')
        except AttributeError as err:
            self.error = err
            return None
        
        p = re.compile(r'(\d{4,})')
        jobs = dict()
        keys = ['name', 'job', 'pm', 'value', 'description', 'client', 'contact']

        for rownum in range(1, ws.nrows):
            jobrow = ws.row_values(rownum, 0, 7)
            jobtypes = ws.row_types(rownum, 0, 7)
            m = p.match(str(jobrow[1]))
            if m:
                jobrow[1] = int(m[0])
                # Ensure the job value is numeric, default to 0.
                if jobtypes[3] != xlrd.XL_CELL_NUMBER:
                    jobrow[3] = 0.0
                jobs[jobrow[1]] = dict(zip(keys, jobrow))
        return jobs


    def get_works_from_worksheet(self, ws):
        self.jobs.clear()
        wb_new_version = False
        if ws.cell_type(0, 7) == xlrd.XL_CELL_TEXT and ws.cell_value(0, 7).startswith('Day_'):
            wb_new_version = True
        block_start = 0

        try:
            # Iterate over the rows to find each job bounds
            for rownum in range(ws.nrows):
                row = ws.row(rownum)
                if (block_start == 0) and str(row[0].value).startswith('CREW'):
                    # Have the start of a crew block
                    block_start = rownum + 2
                elif (block_start > 0) and (ws.cell_type(rownum, 7) == xlrd.XL_CELL_NUMBER) and (ws.cell_value(rownum, 7) > 0):
                    # Have the end of the crew block
                    block_end = rownum
                    ctype = ws.cell_type(block_start, 0)
                    if ctype == xlrd.XL_CELL_NUMBER or ctype == xlrd.XL_CELL_BLANK or ctype == xlrd.XL_CELL_ERROR:
                        job = ''
                    else:
                        job = str(ws.cell_value(block_start, 0))

                    ctype = ws.cell_type(block_start, 1)
                    if ctype == xlrd.XL_CELL_NUMBER:
                        quote = str(int(ws.cell_value(block_start, 1)))
                    elif ctype != xlrd.XL_CELL_TEXT:
                        quote = ''
                    else:
                        quote = str(ws.cell_value(block_start, 1))
                    
                    ctype = ws.cell_type(block_start, 2)
                    if ctype != xlrd.XL_CELL_TEXT:
                        project = ''
                    else:
                        project = ws.cell_value(block_start, 2)

                    crew = list(zip(ws.col_values(7, block_start, block_end),[n or 0.0 for n in ws.col_values(8, block_start, block_end)],ws.col_values(13, block_start, block_end)))
                    # Remove empty items from end of list to produce a more compact display
                    while not crew[-1][0]:
                        crew.pop()
                    
                    if wb_new_version:
                        scope = ws.cell_value(block_start + 6, 2)
                        times = ws.row_values(block_start + 3, 4, 6)
                    else:
                        scope = ws.cell_value(block_start + 1, 3)
                        times = ws.col_values(8, block_end - 2, block_end)
                    # Add in default start and finish times if blank
                    times[0] = times[0] or '06:30'
                    times[1] = times[1] or '15:00'
                    # Possibly convert from time (float) value to string
                    times = list(map(self.totime, times))
                    equip = list(filter(lambda x: x != ('',0.0) and x != ('',''), zip(ws.col_values(6, block_start, block_end), ws.col_values(16, block_start, block_end))))
                    subs = list(filter(lambda x: x != ('',0), zip(ws.col_values(18, block_start, block_end), map(lambda x: 0 if x == '' else int(x), ws.col_values(20, block_start, block_end)))))
                    materials = list(filter(lambda x: x != ('','',''), zip(ws.col_values(23, block_start, block_end), ws.col_values(24, block_start, block_end), ws.col_values(25, block_start, block_end))))
                    self.jobs.append({'job': job, 'quote': quote, 'project': project, 'scope': scope, 'crew': crew, 'times': times, 'equipment': equip, 'subcontractors': subs, 'materials': materials})
                    block_start = 0
                elif (ws.cell_type(rownum, 7) == xlrd.XL_CELL_NUMBER) and (ws.cell_value(rownum, 7) == 0):
                    # Got an empty crew block, reset and keep going, there may be more.
                    block_start = 0
            # Get absentee list
            self.absentees = list(filter(None, ws.col_values(34)[1:39]))
            # Get sheet date as formatted string
            self.date = xlrd.xldate.xldate_as_datetime(ws.cell_value(0,1), self.wb.datemode).date()
        except Exception as error:
            self.error = f'Error loading sheet data. {error}'
        return self.jobs

    def get_works_by_date(self):
        self.jobs.clear()
        if not self.wb:
            self.open_workbook()
            if self.error:
                return self.jobs
                
        self.error = ''
        for sh in self.wb.sheets():
            if sh.nrows > 1 and sh.cell_value(0, 0).startswith('DATE') and (sh.cell_type(0, 1) == xlrd.XL_CELL_DATE):
                sh_date = xlrd.xldate.xldate_as_datetime(sh.cell_value(0,1), self.wb.datemode)
                if sh_date.date() == self.target_date:
                    self.worksheetloaded = sh.name
                    self.get_works_from_worksheet(sh)
                    break
        return self.jobs

    def get_works_from_page(self, targetPage):
        self.jobs.clear()
        if not self.wb:
            self.open_workbook()
            if self.error:
                return self.jobs
                
        self.error = ''
        try:
            ws = self.wb.sheet_by_name(str(targetPage))
        except AttributeError as err:
            self.error = err
            self.jobs.clear()
            return self.jobs
        self.worksheetloaded = ws.name
        return self.get_works_from_worksheet(ws)
    
    def get_revenue(self):
        ''' Get all revenue numbers from the Overview sheet - for all jobs and all days.
        '''
        if not self.wb:
            self.open_workbook()
            if self.error:
                return None
        
        try:
            ws = self.wb.sheet_by_name('Overview')
        except AttributeError as err:
            self.error = err
            return None
        
        colnos = []
        revenue = defaultdict(dict)
        for row in ws.get_rows():
            rowvals = [c.value for c in row]
            rowtypes = [c.ctype for c in row]
            if not colnos:
                if 'Revenue' in rowvals:
                    # Get the Date, job number and revenue column numbers for each of the 4 sections.
                    si = 0
                    for i in range(rowvals.count('Revenue')):
                        colnos.append((rowvals[si:].index('Date') + si, rowvals[si:].index('J/N') + si, rowvals[si:].index('Revenue') + si))
                        si = colnos[-1][2] + 1
            else:
                if rowtypes[colnos[0][0]] == xlrd.XL_CELL_DATE: # Have potential row for extracting info.
                    for dcol, jcol, rcol in colnos:
                        if not rowvals[jcol]:
                            continue
                        rdate = xlrd.xldate.xldate_as_datetime(rowvals[dcol], self.wb.datemode).date()
                        # To handle having multiple job for the same job number on the same day.
                        if rdate in revenue[rowvals[jcol]]:
                            revenue[rowvals[jcol]][rdate] = revenue[rowvals[jcol]][rdate] + (rowvals[rcol] or 0.0)
                        else:
                            revenue[rowvals[jcol]][rdate] = rowvals[rcol] or 0.0
        return revenue

    def alljobids(self):
        return [job['job'] + job['quote'] for job in self.jobs]
    
    def allprojects(self):
        return [job['project'] for job in self.jobs]
    
    def allscopes(self):
        return [job['scope'] for job in self.jobs]

    def allcrews(self):
        return [[name for name,hrs,cost in job['crew']] for job in self.jobs]
    
    def allequipments(self):
        return [[name for name,cost in job['equipment']] for job in self.jobs]
    
    def allsubcontractors(self):
        return [[name for name,cost in job['subcontractors']] for job in self.jobs]

if __name__ == "__main__":
    from sys import exit
    from dwi_export import DWIexport
    from getpass import getpass

    session = {'sharepoint_url': 'https://mayberryconstruct.sharepoint.com/',
        'wksched_path': '/Shared Documents/Works/Works Schedule',
        'fn_format': '/%Y/%-m-%B/Master Planner - %B %y.xlsx'}
    session['sp_username'] = input('Enter Username: ')
    session['sp_password'] = getpass('Enter Password: ')
    works = TZworks(date.today(), session=session)
    if works.error:
        print(works.error)
        exit(1)
    print(works.last_modified)
    rev = works.get_revenue()
    # works.workbook_filename = '/Shared Documents/Works/Construction/Master Templates/Master Planner - GT.xlsx'
    # works.get_works_from_page(1)
    works.get_works_by_date()
    p = DWIexport('L', 'mm', 'A4')
    for job in works.jobs:
        p.add_dwi_page(works.date.strftime("%A, %d %B %Y"), job)
    p.output('dwi_export.pdf', 'F')
    # works.get_works_from_page(10)
    # for job in j:
    #     print('%s%s, %s, %s, (%s), [%s], {%s}' % (job['job'], job['quote'], job['project'], job['scope'], ', '.join(job['crew']), ', '.join(job['equipment']), ', '.join(job['subcontractors'])))
    print(works.date)
    print(','.join(works.alljobids()))
    print(','.join(works.allprojects()))
    print(','.join(works.allscopes()))
    print(repr(works.allcrews()))
    # print(','.join(works.allequipments()))
    # print(','.join(works.allsubcontractors()))
