PRAGMA foreign_keys = ON;

CREATE TABLE job(
    id INTEGER PRIMARY KEY,
    jobname TEXT,
    jobowner TEXT,
    jobvalue FLOAT DEFAULT 0,
    jobrevenue FLOAT DEFAULT 0
);

CREATE TABLE jobday(
    id INTEGER PRIMARY KEY,
    jobdate DATE,
    jobid INTEGER REFERENCES job ON DELETE CASCADE ON UPDATE CASCADE,
    start_time TIME,
    end_time TIME
);

CREATE TABLE crewday(
    id INTEGER PRIMARY KEY,
    crewname TEXT,
    jobdayid INTEGER REFERENCES jobday ON DELETE CASCADE ON UPDATE CASCADE,
    crewhours FLOAT DEFAULT 0,
    cost FLOAT DEFAULT 0
);

CREATE TABLE equipday(
    id INTEGER PRIMARY KEY,
    equipname TEXT,
    jobdayid INTEGER REFERENCES jobday ON DELETE CASCADE ON UPDATE CASCADE,
    cost FLOAT DEFAULT 0
);

CREATE TABLE subcday(
    id INTEGER PRIMARY KEY,
    subcontractor TEXT,
    jobdayid INTEGER REFERENCES jobday ON DELETE CASCADE ON UPDATE CASCADE,
    cost FLOAT DEFAULT 0
);

CREATE TABLE materialday(
    id INTEGER PRIMARY KEY,
    material TEXT,
    jobdayid INTEGER REFERENCES jobday ON DELETE CASCADE ON UPDATE CASCADE,
    amount FLOAT DEFAULT 0,
    cost FLOAT DEFAULT 0
);
