import sqlite3

import click
from flask import current_app, g
from flask.cli import with_appcontext


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row
        g.db.execute('PRAGMA foreign_keys = ON')

    return g.db

def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

def init_db():
    from pathlib import Path
    from cryptography.fernet import Fernet

    # Ensure the database file (and path) exists.
    dbpath = Path(current_app.config['DATABASE'])
    dbpath.parent.mkdir(parents=True, exist_ok=True)
    dbpath.touch(exist_ok=True)
    db = get_db()

    with current_app.open_resource('schema.sql', mode='r') as f:
        db.executescript(f.read())
        db.commit()

    # Create the password key in config.py
    configpath = Path('instance/config.py')
    with configpath.open('w')as f:
        f.write(f"PASSWORD_KEY = {Fernet.generate_key()}\n")

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')

def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
