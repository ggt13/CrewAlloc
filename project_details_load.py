#!/usr/bin/python3

import sys, re
from crewalloc.tz_works import TZworks
from datetime import date, timedelta
from calendar import monthrange
from xlrd import XLRDError, XL_CELL_DATE
from xlrd.xldate import xldate_as_datetime
from cryptography.fernet import Fernet, InvalidToken
import sqlite3

verbose = False
jobnopatt = re.compile(r'^(\d+)')

def load_works_day(c, works, daynum):
    works.get_works_from_page(daynum)
    projects = works.get_all_projects()
    revenue = works.get_revenue()

    for job in works.jobs:

        # Skip blank/invalid job number entries.
        # First extract all leading digits from job number (i.e. ignore trailing letters)
        m = jobnopatt.match(job['quote'])
        if not m:       # No leading digits -> not a usable job
            continue
        job['quote'] = int(m[0])

        # Skip any 'YARD' entries.
        if job['project'] == 'YARD' or not job['quote']:
            continue

        # Insert or update the job.
        c.execute('SELECT * FROM job WHERE id = ? LIMIT 1', [job['quote']])
        j = c.fetchone()
        if not j:
            try:
                c.execute('INSERT INTO job VALUES (?,?,?,?)', (job['quote'], job['project'], job['job'], projects[int(job['quote'])]['value']))
                c.connection.commit()
            except Exception as e:
                print(f'Insert error for job {job["quote"]}: {e}')
        elif j[1] != job['project'] or j[2] != job['job'] or j[3] != projects[j[0]]['value']:
            c.execute('UPDATE job SET jobname = ?, jobowner = ?, jobvalue = ? WHERE id = ?', (job['project'], job['job'], projects[j[0]]['value'], job['quote']))
            c.connection.commit()

        # Insert or update the job's jobday.
        if works.date in revenue[job['quote']]:
            revamt = revenue[job['quote']][works.date]
        else:
            revamt = 0.0
        c.execute('SELECT id from jobday WHERE jobid = ? AND jobdate = ? LIMIT 1', [job['quote'], works.date])
        jd = c.fetchone()
        if not jd:
            c.execute('INSERT INTO jobday(jobdate, jobid, start_time, end_time, revenue) VALUES (?,?,?,?,?)', (works.date,job['quote'],*job['times'],revamt))
            jd = c.lastrowid
        else:
            jd = jd[0]
            c.execute('UPDATE jobday SET start_time = ?, end_time = ?, revenue = ? WHERE id = ?', [*job['times'], revamt, jd])
        c.connection.commit()

        # Insert or update crew information.
        # (Can't easily delete crew members etc. since it's possible to have more than one crew allocated to a job)
        for crew in job['crew']:
            if crew[0]:
                c.execute('SELECT id FROM crewday WHERE jobdayid = ? AND crewname = ? LIMIT 1', [jd, crew[0]])
                cid = c.fetchone()
                if not cid:
                    c.execute('INSERT INTO crewday(jobdayid, crewname, crewhours, cost) VALUES (?,?,?,?)', (jd, *crew))
                else:
                    c.execute('UPDATE crewday SET crewhours = ?, cost = ? WHERE id = ?', (*crew[1:], cid[0]))
                c.connection.commit()
        
        # Insert or update equipment information.
        for equip in job['equipment']:
            if equip[0]:
                c.execute('SELECT id FROM equipday WHERE jobdayid = ? AND equipname = ? LIMIT 1', [jd, equip[0]])
                cid = c.fetchone()
                if not cid:
                    c.execute('INSERT INTO equipday(jobdayid, equipname, cost) VALUES (?,?,?)', (jd, *equip))
                else:
                    c.execute('UPDATE equipday SET cost = ? WHERE id = ?', (equip[1], cid[0]))
                c.connection.commit()
        
        # Insert or update subcontractor information.
        for subc in job['subcontractors']:
            if subc[0]:
                c.execute('SELECT id FROM subcday WHERE jobdayid = ? AND subcontractor = ? LIMIT 1', [jd, subc[0]])
                cid = c.fetchone()
                if not cid:
                    c.execute('INSERT INTO subcday(jobdayid, subcontractor, cost) VALUES (?,?,?)', (jd, *subc))
                else:
                    c.execute('UPDATE subcday SET cost = ? WHERE id = ?', (subc[1], cid[0]))
                c.connection.commit()
        
        # Insert or update material information.
        for material in job['materials']:
            if material[0]:
                c.execute('SELECT id FROM materialday WHERE jobdayid = ? AND material = ? LIMIT 1', [jd, material[0]])
                cid = c.fetchone()
                if not cid:
                    c.execute('INSERT INTO materialday(jobdayid, material, amount, cost) VALUES (?,?,?,?)', (jd, *material))
                else:
                    c.execute('UPDATE materialday SET amount = ?, cost = ? WHERE id = ?', (*material[1:], cid[0]))
                c.connection.commit()
    
def load_workbook(cursor, target_date):
    works = TZworks(target_date, get_settings(cursor), True)
    if works.error:
        print(f"Error getting works details for {target_date}.\nError: {works.error}", file=sys.stderr)
        return
    today = date.today()

    end_day = min(today, target_date.replace(day=monthrange(target_date.year, target_date.month)[1])).day + 1
    for daynum in range(1, end_day):
        if verbose:
            print(daynum)
        load_works_day(cursor, works, daynum)

def load_all_projects(cursor ,target_date):
    works = TZworks(target_date, get_settings(cursor), True)
    if works.error:
        print(f"Error getting works details for {target_date}.\nError: {works.error}", file=sys.stderr)
        return
    projects = works.get_all_projects()
    cursor.execute('SELECT * FROM job')
    dbjobs = cursor.fetchall()
    for job in dbjobs:
        if job[0] in projects and job[3] != projects[job[0]]['value']:
            cursor.execute('UPDATE job SET jobvalue = ? WHERE id = ?', [projects[job[0]]['value'], job[0]])
            cursor.connection.commit()

def get_settings(cursor):
    # cursor.execute('SELECT * FROM settings where setting NOT LIKE "sp_%"')
    cursor.execute('SELECT * FROM settings')
    settings = dict(cursor.fetchall())
    for entry in settings:
        if entry.endswith('assword'):
            f = Fernet(app.config['PASSWORD_KEY'])
            try:
                settings[entry] = f.decrypt(settings[entry].encode()).decode()
            except InvalidToken:
                print('Corrupt encrypted password. Re-enter correct password.', file=sys.stderr)
                return None
    return settings

if __name__ == "__main__":
    ''' By default, get the details from the current month (up to yesterday) and update all
        relevant projects.
    '''
    import argparse
    from crewalloc import app

    # Do argument parsing.
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', help='Show what\'s being done.', action='store_true')
    parser.add_argument('-m', '--month', help='Reload (i.e. delete and recreate) the entire month(s) (specified as YYYY-MM)', nargs='+')
    parser.add_argument('-p', '--project', help='Update project/job values.', action='store_true')
    args = parser.parse_args()
    verbose = args.verbose

    load_dates = None
    if args.month:
        try:
            load_dates = [date.fromisoformat(s + '-01') for s in args.month]
        except ValueError as e:
            print(f'Invalid month. {e}')
            sys.exit(2)

    db_name = app.config['DATABASE']
    with sqlite3.connect(db_name) as conn:
        c = conn.cursor()
        # Ensure foreign key functionality is enabled.
        c.execute('PRAGMA foreign_keys = ON')

        if args.project:
            if verbose:
                print('Loading project values.')
            load_all_projects(c, date.today())
    
        # If specific months are specified (via -m switch) then delete all records for those months.
        if load_dates:
            for target_date in load_dates:
                target_date = str(target_date)[:8] + '%'
                c.execute('DELETE from jobday where jobdate like ?', [target_date])
                c.connection.commit()
        else:
            load_dates = [date.today()]
        for target_date in load_dates:
            if target_date > date.today():
                print(f'Future date {str(target_date)}, skipping.')
                continue
            if verbose:
                print(f'Loading {str(target_date)[:7]}...')
            load_workbook(c, target_date)
    
    if verbose:
        print('Done.')
    sys.exit()

    # SQL commands

    # Get total crew cost for a project:
    #  select sum(cost) from crewday inner join jobday on crewday.jobdayid = jobday.id where jobday.jobid = 5417;
    #
    # Get list of job numbers:
    #  select distinct jobid from jobday where jobdate like "2019-%";