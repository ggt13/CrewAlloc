# Crew Allocation Display

This is a flask application that reads information from a crew allocation spreadsheet and creates
a html file for display on a large screen. In addition to this primary function it also creates
a daily work instructions (DWI) PDF file containing 1 page per work crew which captures the extracted/displayed
information in a form that's suitable for sign-off by the crew leader.

The information extracted and displayed includes:

- Job location, job number
- Project manager
- Crew list (first name taken to be the crew leader)
- Equipment list
- Subcontractors used
- Work instructions

Two ancilliary functions of the application are:

- Display all generated daily work instruction PDF files (linked so they can be displayed/downlaoded)
- Generate a summary for the previous month showing day of month & job number information.

---

## Technical details

The file *crewalloc.py* is the main file for the flask application. It handles the route requests and produces output
pages using the jinja2 templates from the templates directory.

The file *tz_works.py* contains an object that retrieves an excel workbook from sharepoint and loads the job information
for a particular specified day. It caches the data for the day in a sqlite3 database to reduce the number of network
fetches of the workbook.
