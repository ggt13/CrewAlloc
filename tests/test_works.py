#!/usr/bin/python3

import pytest
from datetime import date
from crewalloc.tz_works import TZworks
from crewalloc.db import query_db

def load_settings():
    from cryptography.fernet import Fernet

    settings = dict(query_db('select * from settings'))
    # Decrypt password
    f = Fernet(b'703f6Q5j8vbIu2R1_4cHxIDmudrwXCBlFpRY6JFXHV8=')
    settings['sp_password'] = f.decrypt(settings['sp_password'].encode()).decode()
    print(repr(settings))
    return settings

def test_totime():
    assert TZworks.totime('string') == 'string'
    assert TZworks.totime(4) == 4
    assert TZworks.totime(.5) == '12:00'

def test_works(app):
    with app.app_context():
        w = TZworks(date.today(), loadOnInit=False, session=load_settings())
        assert w.wb == None
        assert w.date == None
        assert w.error == ''
        assert w.workbook_filename.endswith('.xlsx')
        assert len(w.jobs) == 0
        assert w.target_date == date.today()

def test_workbook(app):
    with app.app_context():
        w = TZworks(date.today(), loadOnInit=False, session=load_settings())
        w.open_workbook()
        assert w.wb != None
        assert w.error == ''
        w.get_works_by_date()
        assert w.jobs != None