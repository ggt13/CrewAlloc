from crewalloc import create_app
from crewalloc.db import query_db
from datetime import date, timedelta
from flask import session


def test_config():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing

def test_index(client):
    response = client.get('/')
    assert b'Display Exporter' in response.data
    assert b'Show Today' in response.data
    assert b'Daily Work Instructions' in response.data

def test_logout(client):
    response = client.get('/logout', follow_redirects=True)
    assert b'Logged out' in response.data

def test_badpath(client):
    response = client.get('/syzygy')
    assert b'Error 404' in response.data
    assert b'Page not found!' in response.data

def test_settings1(client):
    response = client.get('/settings')
    assert b'Sharepoint URL' in response.data
    assert b'Works Schedule path' in response.data
    assert b'Sharepoint Username' in response.data
    assert b'Sharepoint Password' in response.data

def test_dwi(client):
    response = client.get('/dwi/')
    assert b'Daily Work Instructions' in response.data
    assert b'.pdf' in response.data
    response = client.get('/dwi')
    assert response.status_code == 301
    assert response.location.endswith('dwi/')
    response = client.get('/dwi', follow_redirects=True)
    assert b'Daily Work Instructions' in response.data

def test_dwi_specific(client):
    with client as c:
        c.get('/')  # This is to populate session with the settings from the database.
        response = c.post('/', data={'target_date': '2019-12-17', 'form-id': 'dwi'}, follow_redirects=True)
        assert response.data.startswith(b'%PDF')

def test_today(client):
    with client as c:
        c.get('/')  # This is to populate session with the settings from the database.
        response = c.post('/', data={'target_day': 'Show Today', 'form-id': 'main'}, follow_redirects=True)
        assert date.today().strftime("%A, %d %B %Y").encode() in response.data
        # This is to induce a cache hit to cover the cache update part of code.
        response = c.post('/', data={'target_day': 'Show Today', 'form-id': 'main'}, follow_redirects=True)

def test_tomorrow(client):
    with client as c:
        c.get('/')  # This is to populate session with the settings from the database.
        response = c.post('/', data={'target_day': 'Show Tomorrow', 'form-id': 'main'}, follow_redirects=True)
        assert (date.today() + timedelta(days=1)).strftime("%A, %d %B %Y").encode() in response.data

def test_monday(client):
    with client as c:
        c.get('/')  # This is to populate session with the settings from the database.
        response = c.post('/', data={'target_day': 'Show Monday', 'form-id': 'main'}, follow_redirects=True)
        assert (date.today() + timedelta(days=(7 - date.today().weekday()))).strftime("%A, %d %B %Y").encode() in response.data

def test_summary(client):
    with client as c:
        c.get('/')  # This is to populate session with the settings from the database.
        response = c.post('/', data={'form-id': 'summary'}, follow_redirects=True)
        assert b'Works Summary' in response.data
        assert b'Summary by TMP/TGS' in response.data
        assert b'Summary by job' in response.data
        assert b'Summary by day' in response.data

def test_settings2(client):
    with client as c:
        c.get('/settings')
        data = dict(query_db('select * from settings'))
        response = c.post('/settings', data=data)
        assert b'Settings saved' in response.data

# Negative test cases.

def test_emptydb(app):
    with app.app_context():
        query_db('delete from settings')
        with app.test_client() as nclient:
            response = nclient.get('/')
            assert response.status_code == 302
            assert response.location.endswith('settings')
            response = nclient.get('/', follow_redirects=True)
            assert b'Please set all items to valid values.' in response.data

def test_bad_sp_url(app):
    with app.app_context():
        query_db('update settings set val = "-" where setting = "sharepoint_url"')
        with app.test_client() as c:
            c.get('/')
            response = c.post('/', data={'target_day': 'Show Today', 'form-id': 'main'}, follow_redirects=True)
            assert b'Error logging in to Sharepoint. Check URL, Username and Password.' in response.data

def test_bad_sp_username(app):
    with app.app_context():
        query_db('update settings set val = "-" where setting = "sp_username"')
        with app.test_client() as c:
            c.get('/')
            assert session['sp_username'] == '-'
            response = c.post('/', data={'target_day': 'Show Today', 'form-id': 'main'}, follow_redirects=True)
            assert b'Error logging in to Sharepoint. Check URL, Username and Password.' in response.data

def test_bad_sp_password(app):
    with app.app_context():
        query_db('update settings set val = "-" where setting = "sp_password"')
        with app.test_client() as c:
            response = c.get('/', follow_redirects=True)
            assert b'Corrupt encrypted password. Re-enter correct password.' in response.data

def test_bad_sp_filename(app):
    with app.app_context():
        query_db('update settings set val = "znonez" where setting = "fn_format"')
        with app.test_client() as c:
            c.get('/')
            assert session['fn_format'] == 'znonez'
            response = c.post('/', data={'target_day': 'Show Today', 'form-id': 'main'}, follow_redirects=True)
            assert b't load workbook named' in response.data
            assert b'znonez' in response.data
